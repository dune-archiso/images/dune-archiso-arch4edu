#+STARTUP: showall
#+TITLE: [[https://wiki.archlinux.org/title/unofficial_user_repositories#arch4edu][dune-archiso-arch4edu]]
#+AUTHOR: Oromion
#+OPTIONS: toc:2

[[https://gitlab.com/dune-archiso/images/dune-archiso-arch4edu/-/pipeline_schedules][Running every friday at 3:00 AM UTC-5]].

1. First, login on GitLab registry.
2. Then, download the image [[https://gitlab.com/dune-archiso/images/dune-archiso-arch4edu/container_registry][dune-archiso-arch4edu]].

#+BEGIN_SRC sh
$ docker images
REPOSITORY                                                             TAG       IMAGE ID       CREATED        SIZE
registry.gitlab.com/dune-archiso/images/dune-archiso-arch4edu   latest    5bc605b1fc04   24 minutes ago   847MB
#+END_SRC