# Copyleft (c) July, 2022, Oromion.
# Usage: docker build -t dune-archiso/dune-archiso:dune-core .

FROM registry.gitlab.com/dune-archiso/images/dune-archiso

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
  name="Dune Arch4edu" \
  description="Dune in Arch4edu" \
  url="https://gitlab.com/dune-archiso/images/dune-archiso-arch4edu/container_registry" \
  vcs-url="https://gitlab.com/dune-archiso/images/dune-archiso-arch4edu" \
  vendor="Oromion Aznarán" \
  version="1.0"

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN curl -s https://gitlab.com/dune-archiso/dune-archiso.gitlab.io/-/raw/main/templates/add_arch4edu.sh | bash

# https://github.com/archlinux/svntogit-packages/blob/packages/pacman/trunk/makepkg.conf
# curl -O https://mirrors.tuna.tsinghua.edu.cn/arch4edu/any/arch4edu-keyring-20200805-2-any.pkg.tar.zst && \
# sha256sum arch4edu-keyring-20200805-2-any.pkg.tar.zst && \
# pacman --noconfirm -U arch4edu-keyring-20200805-2-any.pkg.tar.zst && \
# rm arch4edu-keyring-20200805-2-any.pkg.tar.zst && \
# pacman-key --keyserver hkps://keyserver.ubuntu.com --refresh && \
# pacman-key --refresh-keys && \